import type {ModuleMetadata} from "@nestjs/common";
import {Module as NestModule} from "@nestjs/common/decorators/modules/module.decorator";

/**
 * To instantiate modules, NestJS encourages the usage of classes exposing static functions.
 *
 * @Module({
 *     providers: [Connection],
 * })
 * export class DatabaseModule {
 *     static forRoot(entities = [], options?): DynamicModule {
 *         const providers = createDatabaseProviders(options, entities);
 *         return {
 *             module: DatabaseModule,
 *             providers: providers,
 *             exports: providers,
 *         };
 *     }
 * }
 *
 * NestJS also encourages modules to be empty shells.
 *
 * @Module({
 *     providers: [Connection],
 * })
 * class MainModule {}
 *
 * Here we expose a factory that solves both issues: it doesn't depend on class static functions and,
 * by not being a Class Decorator, it doesn't encourage developers to write empty shells.
 *
 * const MainModule = createModule({
 *     providers: [Connection],
 * });
 */
export const createModule = <M extends ModuleMetadata>(metadata: M) => {
    const target = class {
    };

    NestModule(metadata)(target);

    return target;
};
